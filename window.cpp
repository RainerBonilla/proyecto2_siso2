#ifndef _WINDOW_C_
#define _WINDOW_C_

#include "window.h"
#include <X11/Xlib.h>

string dst_root;

int width;
int height;
char * text[6];
int text_len[6];
int x[6];
int y[6];
bool foundit;
bool focused;
bool focusedFile;
bool focusedDir;
bool focusedDel;
bool focusedCopy;
bool focusedMove;
bool focusedHlink;
bool focusedSlink;

string moveBufferPath;
string moveBufferName;
bool focusedPlace;
bool moveFlag;

/* X Windows related variables. */

Display * display;
int screen;
Window root;
Window window;
GC gc;
XFontStruct * font;
unsigned long black_pixel;    
unsigned long white_pixel;

string openeddir;
string textdir[40];
int xdir[40];
int ydir[40];

string textInput;
int xinput;
int yinput;
unsigned int  widthinput, heightinput;

string textCreateFile;
int xcfile;
int ycfile;
unsigned int  widthcfile, heightcfile;

string textCreateDir;
int xcdir;
int ycdir;
unsigned int  widthcdir, heightcdir;

string textDelete;
int xdel;
int ydel;
unsigned int  widthdel, heightdel;

string textCopy;
int xcop;
int ycop;
unsigned int  widthcop, heightcop;

string textMove;
int xmov;
int ymov;
unsigned int  widthmov, heightmov;

string textHlink;
int xhli;
int yhli;
unsigned int  widthhli, heighthli;

string textSlink;
int xsli;
int ysli;
unsigned int  widthsli, heightsli;

void x_connect()
{
    foundit = false;
    focused = false;
    xinput = 150;
    yinput = 15;
    widthinput = 500;
    heightinput = 20;

    xcfile = 850;
    ycfile = 15;
    widthcfile = 100;
    heightcfile = 40;

    xcdir = 850;
    ycdir = 65;
    widthcdir = 100;
    heightcdir = 40;

    xdel = 850;
    ydel = 115;
    widthdel = 100;
    heightdel = 40;

    xcop = 850;
    ycop = 165;
    widthcop = 100;
    heightcop = 40;

    xmov = 850;
    ymov = 215;
    widthmov = 100;
    heightmov = 40;

    xhli = 850;
    yhli = 265;
    widthhli = 100;
    heighthli = 40;

    xsli = 850;
    ysli = 315;
    widthsli = 100;
    heightsli = 40;

    textCreateFile = "Create File";
    textCreateDir = "Create Dir";
    textDelete = "Delete File/Dir";
    textCopy = "Copy File/Dir";
    textMove = "Move File/dir";
    textHlink = "Create H.link";
    textSlink = "Create S.link";

    struct mntent *m;
    FILE *f;
    printf("_PATH_MOUNTED macro: %s\n", _PATH_MOUNTED);
    f = setmntent(_PATH_MOUNTED, "r");
    int count = 0;
    while ((m = getmntent(f))) {

        char subbuff[5];
        memcpy( subbuff, &m->mnt_dir[0], 4 );
        subbuff[4] = '\0';

        if(strcmp(subbuff,"/dev") == 0 || strcmp(subbuff,"/") == 0){
            printf("Drive: %s      Name: %s\n", m->mnt_dir, m->mnt_fsname);

            text[count] = (char*)malloc(strlen (m->mnt_dir)+1);
            memcpy(text[count],m->mnt_dir,sizeof(m->mnt_dir)+1);
            text_len[count] = strlen (text[count]);
            count = count + 1;
        }
    }
    endmntent(f);

    std::ifstream mountInfo("/proc/mounts");
    while(!mountInfo.eof()){
        Mount each;
        mountInfo >> each.device >> each.destination >> each.fstype >> each.options >> each.dump >> each.pass;
        if(each.device != "" && each.device.substr(0,4) == "/dev"){
            printf("device: %s\n",each.device.c_str());
            printf("destination: %s\n",each.destination.c_str());
        }
    }
    mountInfo.close();


    display = XOpenDisplay(NULL);
    if (! display) {
        fprintf (stderr, "Could not open display.\n");
        exit (1);
    }
    screen = DefaultScreen(display);
    root = RootWindow(display, screen);
    black_pixel = BlackPixel(display, screen);
    white_pixel = WhitePixel(display, screen);
    openeddir = "/";
}

/* Create the window. */

void create_window()
{
    width = 960;
    height = 600;
    window = 
        XCreateSimpleWindow(display,
                             root,
                             1, /* x */
                             1, /* y */
                             width,
                             height,
                             0, /* border width */
                             black_pixel, /* border pixel */
                             white_pixel  /* background */);
    XSelectInput(display, window,
                  ExposureMask | KeyPressMask | KeyReleaseMask | PointerMotionHintMask | ButtonPressMask | ButtonReleaseMask);
    XMapWindow(display, window);
}

/* Set up the GC (Graphics Context). */

void set_up_gc()
{
    screen = DefaultScreen(display);
    gc = XCreateGC(display, window, 0, 0);
    XSetBackground(display, gc, white_pixel); 
    XSetForeground(display, gc, black_pixel); 
}

/* Set up the text font. */

void set_up_font()
{
    const char * fontname = "-*-helvetica-*-r-*-*-14-*-*-*-*-*-*-*";
    font = XLoadQueryFont (display, fontname);
    /* If the font could not be loaded, revert to the "fixed" font. */
    if (! font) {
        fprintf (stderr, "unable to load font %s: using fixed\n", fontname);
        font = XLoadQueryFont (display, "fixed");
    }
    XSetFont (display, gc, font->fid);
}

/* Draw the window. */

void draw_screen()
{
    int direction;
    int ascent;
    int descent;
    XCharStruct overall;

    /* Centre the text in the middle of the box. */

    XTextExtents(font, text[0], text_len[0],
                  & direction, & ascent, & descent, & overall);
    x[0] = 10;
    y[0] = 15;
    XClearWindow(display, window);

    XDrawString(display, window, gc,
                x[0], y[0], text[0], text_len[0]);


    for(int i =1; i<6; i++){
        XTextExtents(font, text[i], text_len[i],
                  & direction, & ascent, & descent, & overall);
        y[i] = y[i-1] + 25;
        x[i] = 10;

        XDrawString(display, window, gc,
                    x[i], y[i], text[i], text_len[i]);
    }

    DIR *dir;
    struct dirent *ent;

    if ((dir = opendir (openeddir.c_str())) != NULL) {
        printf("in dir...%s\n", openeddir.c_str());
    /* print all the files and directories within directory */
    int x = 170;
    int y = 35;
    int counter = 0;

    while ((ent = readdir (dir)) != NULL) {
        char * hola =ent->d_name;
        if(hola[0] != '.'){
            string temp = ent->d_name;
            /*printf ("%s\n", ent->d_name);
            printf("check entry...\n");
            string temp = ent->d_name;
            printf("entry: %s\n",ent->d_name);
            printf("...entry checked\n");
            */
            /*strcat(temp, "/");*/
            /*strcat(temp,ent->d_name);

            if(is_regular_file(temp)){
                printf ("\n");
            }
            else{
                strcat(temp,"/");
                printf ("/\n");
            }
            */
            XTextExtents(font, temp.c_str(), strlen(temp.c_str()),
                    & direction, & ascent, & descent, & overall);

            if(counter==19){
                x = x + 280;
                y = 35 + 25;
            }
            else{
                y = y + 25;
            }

            XDrawString(display, window, gc,
                            x, y, temp.c_str(), strlen(temp.c_str()));
            
            string regg;
            if(!openeddir.compare("/")){
                regg = openeddir + temp;
            }else{
                regg = openeddir + "/" + temp;
            }
            printf("REGG: %s\n",regg.c_str());
            if(!is_regular_file(regg.c_str())){
                string icon = "[D]";
                XTextExtents(font, icon.c_str(), strlen(icon.c_str()),
                    & direction, & ascent, & descent, & overall);
                
                XDrawString(display, window, gc,
                            x-20, y, icon.c_str(), strlen(icon.c_str()));
                
            }else{
                struct stat buf;
                int wq;

                wq = lstat (regg.c_str(), &buf);
                if (S_ISLNK(buf.st_mode)){
                    string icon = "L~";
                    XTextExtents(font, icon.c_str(), strlen(icon.c_str()),
                        & direction, & ascent, & descent, & overall);
                    
                    XDrawString(display, window, gc,
                                x-20, y, icon.c_str(), strlen(icon.c_str()));
                }
                else{
                    string icon = "-";
                    XTextExtents(font, icon.c_str(), strlen(icon.c_str()),
                        & direction, & ascent, & descent, & overall);
                    
                    XDrawString(display, window, gc,
                                x-20, y, icon.c_str(), strlen(icon.c_str()));
                }
            }
            if(counter<40){
                textdir[counter] = temp;
                xdir[counter] = x;
                ydir[counter] = y;
                counter++;
            }
        }
    }
    closedir (dir);
    } else {
        printf ("no se pudo\n");
    }
    printf("...end dir\n");

    XDrawRectangle(display, window, gc, xinput, yinput, widthinput,heightinput);
    XDrawRectangle(display, window, gc, xinput, yinput+520, widthinput+200,heightinput);

    string path = "PATH:";
    XTextExtents(font, path.c_str(), strlen(path.c_str()),
        & direction, & ascent, & descent, & overall);
    
    XDrawString(display, window, gc,
                xinput-40, yinput+535, path.c_str(), strlen(path.c_str()));

    XTextExtents(font, openeddir.c_str(), strlen(openeddir.c_str()),
        & direction, & ascent, & descent, & overall);
    
    XDrawString(display, window, gc,
                xinput+5, yinput+535, openeddir.c_str(), strlen(openeddir.c_str()));

    if(focused){
        XDrawLine(display, window, gc, xinput-5, yinput, xinput-5, yinput+20);

        XTextExtents(font, textInput.c_str(), strlen(textInput.c_str()),
                    & direction, & ascent, & descent, & overall);

        XDrawString(display, window, gc,
                        xinput+2, yinput+18, textInput.c_str(), strlen(textInput.c_str()));
    }

    XDrawRectangle(display, window, gc, xcfile, ycfile, widthcfile,heightcfile);
    XDrawRectangle(display, window, gc, xcdir, ycdir, widthcdir,heightcdir);
    XDrawRectangle(display, window, gc, xdel, ydel, widthdel,heightdel);
    XDrawRectangle(display, window, gc, xcop, ycop, widthcop,heightcop);
    XDrawRectangle(display, window, gc, xmov, ymov, widthmov,heightmov);
    XDrawRectangle(display, window, gc, xhli, yhli, widthhli,heighthli);
    XDrawRectangle(display, window, gc, xsli, ysli, widthsli,heightsli);

    XTextExtents(font, textCreateFile.c_str(), strlen(textCreateFile.c_str()),
                    & direction, & ascent, & descent, & overall);
    XDrawString(display, window, gc,
                    xcfile+2, ycfile+18, textCreateFile.c_str(), strlen(textCreateFile.c_str()));

    XTextExtents(font, textCreateDir.c_str(), strlen(textCreateDir.c_str()),
                    & direction, & ascent, & descent, & overall);
    XDrawString(display, window, gc,
                    xcdir+2, ycdir+18, textCreateDir.c_str(), strlen(textCreateDir.c_str()));

    XTextExtents(font, textDelete.c_str(), strlen(textDelete.c_str()),
                    & direction, & ascent, & descent, & overall);
    XDrawString(display, window, gc,
                    xdel+2, ydel+18, textDelete.c_str(), strlen(textDelete.c_str()));

    XTextExtents(font, textCopy.c_str(), strlen(textCopy.c_str()),
                    & direction, & ascent, & descent, & overall);
    XDrawString(display, window, gc,
                    xcop+2, ycop+18, textCopy.c_str(), strlen(textCopy.c_str()));

    XTextExtents(font, textMove.c_str(), strlen(textMove.c_str()),
                    & direction, & ascent, & descent, & overall);
    XDrawString(display, window, gc,
                    xmov+2, ymov+18, textMove.c_str(), strlen(textMove.c_str()));

    XTextExtents(font, textHlink.c_str(), strlen(textHlink.c_str()),
                    & direction, & ascent, & descent, & overall);
    XDrawString(display, window, gc,
                    xhli+2, yhli+18, textHlink.c_str(), strlen(textHlink.c_str()));

    XTextExtents(font, textSlink.c_str(), strlen(textSlink.c_str()),
                    & direction, & ascent, & descent, & overall);
    XDrawString(display, window, gc,
                    xsli+2, ysli+18, textSlink.c_str(), strlen(textSlink.c_str()));
}

int is_regular_file(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}

/* Loop over events. */

void event_loop()
{
    XEvent e;
    while (1) {
        XNextEvent(display, & e);
        printf("x- %d\n",e.xbutton.x);
        printf("y- %d\n",e.xbutton.y);

        setxtextInput(e,display);
        setxcreateFile(e, display);
        setxcreateDir(e, display);
        setxDelete(e, display);
        setxCopy(e, display);
        setxMove(e, display);
        setxHlink(e, display);
        setxSlink(e, display);

        if(e.xbutton.x >= xdel && e.xbutton.x <= xdel+widthdel){
            if(e.xbutton.y >= ydel && e.xbutton.y <= ydel+heightdel){
                focusedDel = true;
            }
            else{
                focusedDel = false;
            }
        }
        else{
            focusedDel = false;
        }

        if(e.xbutton.x >= xcfile && e.xbutton.x <= xcfile+widthcfile){
            if(e.xbutton.y >= ycfile && e.xbutton.y <= ycfile+heightcfile){
                focusedFile = true;
            }
            else{
                focusedFile = false;
            }
        }
        else{
            focusedFile = false;
        }

        if(e.xbutton.x >= xcdir && e.xbutton.x <= xcdir+widthcdir){
            if(e.xbutton.y >= ycdir && e.xbutton.y <= ycdir+heightcdir){
                focusedDir = true;
            }
            else{
                focusedDir = false;
            }
        }
        else{
            focusedDir = false;
        }

        if(e.xbutton.x >= xinput && e.xbutton.x <= xinput+widthinput){
            if(e.xbutton.y >= yinput && e.xbutton.y <= yinput+heightinput){
                focused = true;
            }
            else{
                focused = false;
            }
        }
        else{
            focused = false;
        }

        if(e.xbutton.x >= xcop && e.xbutton.x <= xcop+widthcop){
            if(e.xbutton.y >= ycop && e.xbutton.y <= ycop+heightcop){
                focusedCopy = true;
            }
            else{
                focusedCopy = false;
            }
        }
        else{
            focusedCopy = false;
        }

        if(e.xbutton.x >= xmov && e.xbutton.x <= xmov+widthmov){
            if(e.xbutton.y >= ymov && e.xbutton.y <= ymov+heightmov){
                if(moveFlag){
                    focusedPlace = true;
                }
                else
                {
                    focusedMove = true;
                }
                
            }
            else{
                focusedMove = false;
                focusedPlace = false;
            }
        }
        else{
            focusedMove = false;
            focusedPlace = false;
        }

        if(e.xbutton.x >= xsli && e.xbutton.x <= xsli+widthsli){
            if(e.xbutton.y >= ysli && e.xbutton.y <= ysli+heightsli){
                focusedSlink = true;
            }
            else{
                focusedSlink = false;
            }
        }
        else{
            focusedSlink = false;
        }

        if(e.xbutton.x >= xhli && e.xbutton.x <= xhli+widthhli){
            if(e.xbutton.y >= yhli && e.xbutton.y <= yhli+heighthli){
                focusedHlink = true;
            }
            else{
                focusedHlink = false;
            }
        }
        else{
            focusedHlink = false;
        }

        if(!foundit){
            for(int i=0; i<6;i++){
                if(e.xbutton.x >= x[i] && e.xbutton.x <= 130){
                    if(e.xbutton.y >= y[i]-3 && e.xbutton.y <= y[i]+3){
                        foundit=true;
                        openeddir = text[i];
                        printf("dir selected- %s\n",openeddir.c_str());
                        break;
                    }
                }
            }

            for(int i=0; i<40;i++){
                if(e.xbutton.x >= xdir[i] && e.xbutton.x <= xdir[i]+100){
                    if(e.xbutton.y >= ydir[i]-3 && e.xbutton.y <= ydir[i]+3){

                        foundit=true;
                        printf("clicked on: %s with path: %s\n",textdir[i].c_str(), openeddir.c_str());
                        string path;
                        if(strlen("/")== strlen(openeddir.c_str())){
                            path = openeddir+textdir[i];
                        }
                        else{
                            path = openeddir+"/"+textdir[i];
                        }

                        if(!is_regular_file(path.c_str())){
                            printf("is dir: %s\n",path.c_str());
                            
                            openeddir = path;
                            printf("dir selected- %s\n",openeddir.c_str());
                        }
                        else{
                            printf("not dir\n");
                            string tempo = "gio open ";
                            tempo = tempo + path;
                            system(tempo.c_str());
                        }
                        break;
                    }
                }
            }
            memset(textdir, 0, sizeof(textdir));
            memset(xdir, 0, sizeof(xdir));
            memset(ydir, 0, sizeof(ydir));
        }
        else{
            foundit = false;
        }
        draw_screen();
    }
}

void freeStr(char **str)
{
    free( *str );
    *str = NULL;   
}

void setxtextInput(XEvent e, Display *dpy)
{
    if(focused)
    {
        printf("In rectangle!\n");
        if(e.type == KeyPress)
        {
            KeySym id = XkbKeycodeToKeysym(dpy,e.xkey.keycode,0,
                e.xkey.state & ShiftMask ? 1 : 0);
            char stringData[1];
            XComposeStatus x;
            XLookupString(&e.xkey,stringData,1,&id,&x);
            if(id == 65288)
            {
                if(textInput.size()>0){
                    textInput = textInput.substr(0, textInput.size()-1);
                }
            }
            else
            {
                textInput = textInput + stringData;
            }
            

        }
    }
}

void setxcreateFile(XEvent e, Display *dpy){
    if(focusedFile){
        printf("In createFile!\n");
        if(textInput.length() !=0){
            int fd;
            mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
            string tempo = openeddir + "/" + textInput;
            printf(" with input: %s\n",tempo.c_str());

            fd = creat(tempo.c_str(), mode);
            if(fd == -1){
                printf("an error occured\n");
            }
        }
        else{
            printf("\n");
        }


        textInput.clear();
    }
}

void setxcreateDir(XEvent e, Display *dpy){
    if(focusedDir){
        printf("In createDir!\n");
        if(textInput.length() !=0){

            string tempo = openeddir + "/" + textInput;
            printf(" with input: %s\n",tempo.c_str());

            int status;
            status = mkdir(tempo.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

            if(status == -1){
                printf("an error occured\n");
            }
        }
        else{
            printf("\n");
        }


        textInput.clear();
    }
}

void setxDelete(XEvent e, Display *dpy){
    if(focusedDel){
        printf("In Delete!\n");
        if(textInput.length() !=0){

            string tempo = openeddir + "/" + textInput;
            printf(" with input: %s\n",tempo.c_str());

            if(!is_regular_file(tempo.c_str())){
                printf("is dir: %s\n",tempo.c_str());

                int status = remove_directory(tempo.c_str());
                if(status == -1){
                    printf("an error occured\n");
                }
            }
            else{
                printf("not dir\n");

                int status= remove(tempo.c_str()); 

                if(status == -1){
                    printf("an error occured\n");
                }

            }
        }
        else{
            printf("\n");
        }

        textInput.clear();
    }
}

int remove_directory(const char *path)
{
   DIR *d = opendir(path);
   size_t path_len = strlen(path);
   int r = -1;

   if (d)
   {
      struct dirent *p;
      r = 0;
      while (!r && (p=readdir(d)))
      {
          int r2 = -1;
          char *buf;
          size_t len;

          /* Skip the names "." and ".." as we don't want to recurse on them. */
          if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
          {
             continue;
          }
          len = path_len + strlen(p->d_name) + 2; 
          buf = (char*)malloc(len);
          if (buf)
          {
             struct stat statbuf;
             snprintf(buf, len, "%s/%s", path, p->d_name);
             if (!stat(buf, &statbuf))
             {
                if (S_ISDIR(statbuf.st_mode))
                {
                   r2 = remove_directory(buf);
                }
                else
                {
                   r2 = unlink(buf);
                }
             }

             free(buf);
          }
          r = r2;
      }
      closedir(d);
   }
   if (!r)
   {
      r = rmdir(path);
   }
   return r;
}

void setxCopy(XEvent e, Display *dpy){
    if(focusedCopy){
        printf("In copyFile!\n");
        if(textInput.length() !=0){

            string tempo = openeddir + "/" + textInput;
            printf(" with input: %s\n",tempo.c_str());

            if(IsPathExist(tempo)){
                if(!is_regular_file(tempo.c_str())){
                    printf("is dir: %s\n",tempo.c_str());

                    string tempi = tempo;
                    do{
                        tempi = tempi + "_copy";
                    }while(IsPathExist(tempi));

                    printf("new copied dir: %s\n",tempi.c_str());

                    fs::copy(tempo, tempi, fs::copy_options::overwrite_existing | fs::copy_options::recursive);

                    printf("done copying\n");
                }
                else{
                    printf("not dir\n");

                    istringstream iss(tempo);
                    string tokens[2];
                    string token;
                    int i=0;
                    while (std::getline(iss, token, '.')) {
                        if (!token.empty()){
                            tokens[i] = token;
                            i++;
                        }
                    }
                    printf("full: %s\n",tempo.c_str());
                    printf("token a: %s\n",tokens[0].c_str());
                    printf("token b: %s\n",tokens[1].c_str());

                    string tempi = tokens[0];
                    string temper;
                    do{
                        tempi = tempi + "_copy"; 
                        temper = tempi + "." + tokens[1];
                    }while(IsPathExist(temper));

                    printf("new copied file: %s\n",temper.c_str());

                    std::ifstream  src(tempo, std::ios::binary);
                    std::ofstream  dst(temper,   std::ios::binary);
                    dst << src.rdbuf();

                    printf("done copying\n");

                }
            }

        }
        else{
            printf("\n");
        }


        textInput.clear();
    }
}

void setxMove(XEvent e, Display *dpy){
    if(focusedMove){
        printf("In moveFile!\n");
        if(textInput.length() !=0){
            
            string tempo = openeddir + "/" + textInput;
            printf(" with input: %s\n",tempo.c_str());

            if(IsPathExist(tempo)){
                if(!is_regular_file(tempo.c_str())){
                    printf("is dir: %s\n",tempo.c_str());
                    moveBufferName = textInput;
                    moveBufferPath = tempo;
                    moveFlag = true;
                    textMove = "Place File/Dir";

                }
                else{
                    printf("not dir\n");

                    moveBufferName = textInput;
                    moveBufferPath = tempo;
                    moveFlag = true;
                    textMove = "Place File/Dir";
                }
            }

        }
        else{
            printf("\n");
        }


        textInput.clear();
    }
    else if(focusedPlace){
        printf("OG path: %s\n",moveBufferPath.c_str());
        printf("this file/Dir: %s  in this path: %s\n",moveBufferName.c_str(),openeddir.c_str());

        if(!is_regular_file(moveBufferPath.c_str())){
            printf("is dir\n");

            moveBufferName = openeddir + "/" + moveBufferName;

            fs::copy(moveBufferPath, moveBufferName, fs::copy_options::overwrite_existing | fs::copy_options::recursive);

            int status = remove_directory(moveBufferPath.c_str());
            if(status == -1){
                printf("an error occured\n");
            }


        }
        else{
            printf("not dir\n");

            moveBufferName = openeddir + "/" + moveBufferName;

            std::ifstream  src(moveBufferPath, std::ios::binary);
            std::ofstream  dst(moveBufferName,   std::ios::binary);
            dst << src.rdbuf();

            int status = remove(moveBufferPath.c_str());
            if(status == -1){
                printf("an error occured\n");
            }

        }

        moveBufferName.clear();
        moveBufferPath.clear();

        moveFlag = false;
        textMove = "Move File/dir";

    }
}

void setxHlink(XEvent e, Display *dpy){
    if(focusedHlink){
        printf("In hLink!\n");
        if(textInput.length() !=0){
            
            string tempo = openeddir + "/" + textInput;
            printf(" with input: %s\n",tempo.c_str());

            if(IsPathExist(tempo.c_str())){

                string tempi = tempo;
                do{
                    tempi = tempi + "_hardlink";
                }while(IsPathExist(tempi));


                int status = link(tempo.c_str(),tempi.c_str());
                
                if(status == -1){
                    printf("an error occured\n");
                }

            }

        }
        else{
            printf("\n");
        }


        textInput.clear();
    }
}

void setxSlink(XEvent e, Display *dpy){
    if(focusedSlink){
        printf("In sLink!\n");
        if(textInput.length() !=0){
            
            string tempo = openeddir + "/" + textInput;
            printf(" with input: %s\n",tempo.c_str());

            if(IsPathExist(tempo.c_str())){

                string tempi = tempo;
                do{
                    tempi = tempi + "_symbolink";
                }while(IsPathExist(tempi));


                int status = symlink(tempo.c_str(),tempi.c_str());

                if(status == -1){
                    printf("an error occured\n");
                }

            }

        }
        else{
            printf("\n");
        }


        textInput.clear();
    }
}

bool IsPathExist(const std::string &s)
{
  struct stat buffer;
  return (stat (s.c_str(), &buffer) == 0);
}

int copy_file(const char* src_path, const struct stat* sb, int typeflag) {
    std::string dst_path = dst_root + src_path;
    switch(typeflag) {
    case FTW_D:
        mkdir(dst_path.c_str(), sb->st_mode);
        break;
    case FTW_F:
        std::ifstream  src(src_path, std::ios::binary);
        std::ofstream  dst(dst_path, std::ios::binary);
        dst << src.rdbuf();
    }
    return 0;
}


#endif /* _WINDOW_C_ */