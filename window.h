#ifndef _WINDOW_H_
#define _WINDOW_H_

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <X11/Xlib.h>
#include <mntent.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <fstream>
#include <X11/XKBlib.h>
#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include <fcntl.h>
#include <ftw.h>
#include <sstream>
#include <experimental/filesystem>
#include <functional>
namespace fs = std::experimental::filesystem;

using namespace std;

struct Mount{
    std::string device;
    std::string destination;
    std::string fstype;
    std::string options;
    int dump;
    int pass;
};

/* The window which contains the text. */

void x_connect();
void create_window();
void set_up_gc();
void set_up_font();
void draw_screen();
void event_loop();
int is_regular_file(const char *path);
void freeStr(char **str);
void setxtextInput(XEvent e, Display *dpy);
void setxcreateFile(XEvent e, Display *dpy);
void setxcreateDir(XEvent e, Display *dpy);
void setxDelete(XEvent e, Display *dpy);
int remove_directory(const char *path);
void setxCopy(XEvent e, Display *dpy);
void setxMove(XEvent e, Display *dpy);
void setxHlink(XEvent e, Display *dpy);
void setxSlink(XEvent e, Display *dpy);

bool IsPathExist(const std::string &s);
int copy_file(const char* src_path, const struct stat* sb, int typeflag);

#endif /* _WINDOW_H_ */
